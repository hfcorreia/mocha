package prompt

import (
	"fmt"
	"strconv"

	"github.com/manifoldco/promptui"
)

func Prompt(label string) (string, error) {
	result, err := prompt(label, "", EmptyStringValidator)
	if err != nil {
		return "", err
	}

	return result, nil
}

func PromptInt(label string, defaultVal *int) (int, error) {
	defaultValueStr := ""
	if defaultVal != nil {
		defaultValueStr = strconv.Itoa(*defaultVal)
	}

	result, err := prompt(label, defaultValueStr, IntValidator)
	if err != nil {
		return 0, err
	}

	return strconv.Atoi(result)
}

func PromptFloat(label string, defaultVal *float64) (float64, error) {
	defaultValueStr := ""
	if defaultVal != nil {
		defaultValueStr = fmt.Sprintf("%f", *defaultVal)
	}

	result, err := prompt(label, defaultValueStr, FloatValidator)
	if err != nil {
		return 0, err
	}

	return strconv.ParseFloat(result, 64)
}

func prompt(label, defaultVal string, validor func(string) error) (string, error) {
	prompt := promptui.Prompt{
		Label:   label,
		Default: defaultVal,
	}
	if validor != nil {
		prompt.Validate = validor
	}

	result, err := prompt.Run()
	if err != nil {
		return "", err
	}

	return result, nil
}
