package prompt

import (
	"fmt"

	"github.com/manifoldco/promptui"
)

func Select(label string, selectOptions ...string) (string, error) {
	if selectOptions == nil || len(selectOptions) == 0 {
		return "", fmt.Errorf("no options provided to select prompt")
	}

	prompt := promptui.Select{
		Label: label,
		Items: selectOptions,
	}

	_, selected, err := prompt.Run()
	if err != nil {
		return "", err
	}

	return selected, nil
}
