package prompt

import (
	"fmt"
	"strconv"
)

func FloatValidator(input string) error {
	value, err := strconv.ParseFloat(input, 64)
	if err != nil {
		return fmt.Errorf("Number must be an int")
	}
	if value <= 0 {
		return fmt.Errorf("Number has to be positive")
	}

	return nil
}

func IntValidator(input string) error {
	value, err := strconv.ParseInt(input, 10, 64)
	if err != nil {
		return fmt.Errorf("Number must be an int")
	}
	if value <= 0 {
		return fmt.Errorf("Number has to be positive")
	}

	return nil
}

func EmptyStringValidator(input string) error {
	if len(input) == 0 || input == "" {
		return fmt.Errorf("Invalid length")
	}

	return nil
}
