package main

import (
	"bitbucket.org/hfcorreia/mocha/cmd/app/seed"
	"fmt"
	"time"

	"bitbucket.org/hfcorreia/mocha"
	"bitbucket.org/hfcorreia/mocha/cmd/app/admin"
	"bitbucket.org/hfcorreia/mocha/cmd/app/customer"
	"bitbucket.org/hfcorreia/mocha/repository"
	"github.com/gomodule/redigo/redis"
	"github.com/spf13/cobra"
)

func main() {
	pool := startRedisPool()

	// repos
	productRepo := repository.NewRepository("products", pool)
	changeRepo := repository.NewRepository("change", pool)

	// service
	productService := mocha.NewProductService(productRepo)
	changeService := mocha.NewChangeService(changeRepo)
	paymentService := mocha.NewPaymentService(productService, changeService)

	services := mocha.Services{productService, changeService, paymentService}

	cmd := setupCmds(services)
	cmd.Execute()
}

func setupCmds(services mocha.Services) *cobra.Command {
	adminCmd := &cobra.Command{
		Use:   "admin",
		Short: "run mocha as admin",
		Run: func(cmd *cobra.Command, args []string) {
			err := admin.Run(services)
			if err != nil {
				fmt.Println(err)
			}
		},
	}
	customerCmd := &cobra.Command{
		Use:   "customer",
		Short: "run mocha as customer",
		Run: func(cmd *cobra.Command, args []string) {
			err := customer.Run(services)
			if err != nil {
				fmt.Println(err)
			}
		},
	}
	loaderCmd := &cobra.Command{
		Use:   "load",
		Short: "load seed",
		Run: func(cmd *cobra.Command, args []string) {
			err := seed.Run(services)
			if err != nil {
				fmt.Println(err)
			}
		},
	}

	rootCmd := &cobra.Command{Use: "app"}
	rootCmd.AddCommand(adminCmd)
	rootCmd.AddCommand(customerCmd)
	rootCmd.AddCommand(loaderCmd)

	return rootCmd
}

func startRedisPool() *redis.Pool {
	return &redis.Pool{
		MaxIdle:     3,
		IdleTimeout: 240 * time.Second,
		Dial: func() (redis.Conn, error) {
			return redis.Dial("tcp", "db:6379")
		},
	}
}
