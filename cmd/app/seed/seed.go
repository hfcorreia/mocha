package seed

import (
	"bitbucket.org/hfcorreia/mocha"
	"fmt"
)

type Seed struct {
	Change   float64
	Products []mocha.Product
}

func Run(services mocha.Services) error {
	fmt.Println("seeding db")

	seed := fetch()
	for _, product := range seed.Products {
		fmt.Printf("creating %v product\n", product)
		err := services.Products.Update(product)
		if err != nil {
			return err
		}
	}

	fmt.Printf("setting initial change %v\n", seed.Change)
	err := services.Change.SetInitialChange(seed.Change)
	if err != nil {
		return err
	}

	fmt.Println("seeded db")

	return nil
}

func fetch() Seed {
	return Seed{
		Change: 1000,
		Products: []mocha.Product{
			{
				"Coffee",
				1.5,
				100,
			},
			{
				"Tea",
				2,
				90,
			},
			{
				"Water",
				0.01,
				100,
			},
			{
				"Coke",
				0.5,
				10,
			},
		},
	}
}
