package product

import (
	"fmt"

	"bitbucket.org/hfcorreia/mocha"
	"bitbucket.org/hfcorreia/mocha/cmd/prompt"
)

const (
	Create = "Create a new product"
	Update = "Update an existing"
	List   = "List all products"
)

func Run(service mocha.ProductService) error {
	selection, err := prompt.Select("Please select option: ", Create, Update, List)
	if err != nil {
		return err
	}

	if selection == Create {
		return CreateProduct(service)
	}

	if selection == Update {
		return UpdateProduct(service)
	}

	if selection == List {
		return ListProducts(service)
	}

	return nil
}

func CreateProduct(service mocha.ProductService) error {
	name, err := prompt.Prompt("Product name")
	if err != nil {
		return err
	}

	price, err := prompt.PromptFloat("Product Price", nil)
	if err != nil {
		return err
	}

	quantity, err := prompt.PromptInt("Product Quantity", nil)
	if err != nil {
		return err
	}

	if err := service.Create(name, price, quantity); err != nil {
		return err
	}

	fmt.Printf("successfully created %s with price %.2f!\n", name, price)

	return nil
}

func UpdateProduct(service mocha.ProductService) error {
	name, err := prompt.Prompt("Enter the name of the product you want to update")
	if err != nil {
		return err
	}

	product, err := service.Get(name)
	if err != nil {
		return err
	}

	fmt.Printf("%s current price is %.2f\n", name, product.Price)
	fmt.Printf("%s current quantity is %d\n", name, product.Quantity)

	price, err := prompt.PromptFloat("New Price", &product.Price)
	if err != nil {
		return err
	}

	quantity, err := prompt.PromptInt("New Quantity", &product.Quantity)
	if err != nil {
		return err
	}

	product.Price = price
	product.Quantity = quantity

	err = service.Update(product)
	if err != nil {
		return err
	}

	return nil
}

func ListProducts(service mocha.ProductService) error {
	products, err := service.GetAll()
	if err != nil {
		return err
	}

	for _, product := range products {
		fmt.Printf("%s:\tPrice: %.2f\tQuantity: %d\n", product.Name, product.Price, product.Quantity)
	}

	return nil
}
