package change

import (
	"fmt"

	"bitbucket.org/hfcorreia/mocha"
	"bitbucket.org/hfcorreia/mocha/cmd/prompt"
)

const (
	Add = "Add change"
)

func Run(service mocha.ChangeService) error {
	currentChange, err := service.Get()
	if err != nil {
		return err
	}

	label := fmt.Sprintf("Available change is %.2f", currentChange)
	selection, err := prompt.Select(label, Add)
	if err != nil {
		return err
	}

	if Add == selection {
		return AddChange(service)
	}

	return nil
}

func AddChange(service mocha.ChangeService) error {
	var change mocha.Change

	for {
		amount, err := prompt.PromptFloat("Amount ", nil)
		if err != nil {
			return err
		}

		change, err = mocha.NewChange(amount)
		if err != nil {
			fmt.Println(err)
		}
		break
	}

	return service.AddChange(change)
}
