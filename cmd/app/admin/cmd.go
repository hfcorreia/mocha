package admin

import (
	"bitbucket.org/hfcorreia/mocha"
	"bitbucket.org/hfcorreia/mocha/cmd/app/admin/change"
	"bitbucket.org/hfcorreia/mocha/cmd/app/admin/product"
	"bitbucket.org/hfcorreia/mocha/cmd/prompt"
)

const (
	ProductAction = "Manage Products"
	ChangeAction  = "Manage Change"
	Exit          = "Exit"
)

func Run(services mocha.Services) error {
	selected, err := prompt.Select("Please select an action ", ProductAction, ChangeAction, Exit)
	if err != nil {
		return err
	}

	if selected == ProductAction {
		return product.Run(services.Products)
	}

	if selected == ChangeAction {
		return change.Run(services.Change)
	}

	return nil
}
