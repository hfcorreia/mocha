package customer

import (
	"fmt"

	"bitbucket.org/hfcorreia/mocha"
	"bitbucket.org/hfcorreia/mocha/cmd/prompt"
)

func Run(services mocha.Services) error {
	availableProducts, err := services.Products.GetAll()
	if err != nil {
		return fmt.Errorf("could't fetch available products %s", err)
	}

	if len(availableProducts) == 0 {
		return fmt.Errorf("sorry but we are out of products for you to buy")
	}

	selectedProduct, err := prompt.Select("Please select the product to buy ", getNames(availableProducts)...)
	if err != nil {
		return err
	}

	product, err := findProduct(selectedProduct, availableProducts)
	if err != nil {
		return err
	}

	label := fmt.Sprintf("%s price is %.2f. Please enter money: ", selectedProduct, product.Price)
	amount, err := prompt.PromptFloat(label, nil)
	if err != nil {
		return err
	}

	change, err := mocha.NewChange(amount)
	if err != nil {
		return err
	}

	remaindingChange, err := services.Payment.Pay(product, change)
	if err != nil {
		return err
	}

	fmt.Printf("Getting %s for you\n", selectedProduct)
	if remaindingChange != 0 {
		fmt.Printf("Remainding change %.2f\n", remaindingChange)
	}

	return nil
}

func findProduct(productName string, products []mocha.Product) (mocha.Product, error) {
	for _, product := range products {
		if product.Name == productName {
			return product, nil
		}
	}

	return mocha.Product{}, fmt.Errorf("could not find product with name %s", productName)
}

func getNames(products []mocha.Product) []string {
	var productNames []string
	for _, product := range products {
		if product.Quantity > 0 {
			productNames = append(productNames, product.Name)
		}
	}

	return productNames
}
