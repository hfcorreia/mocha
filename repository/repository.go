package repository

import (
	"fmt"

	"bitbucket.org/hfcorreia/mocha"
	"github.com/gomodule/redigo/redis"
)

type repository struct {
	pool      *redis.Pool
	keyPrefix string
}

func NewRepository(keyPrefix string, pool *redis.Pool) mocha.Repository {
	return repository{
		pool:      pool,
		keyPrefix: keyPrefix,
	}
}

func (repo repository) Set(id string, value []byte) error {
	connection := repo.pool.Get()
	defer connection.Close()

	key := repo.key(id)
	_, err := connection.Do("SET", key, value)
	if err != nil {
		return err
	}

	return err
}

func (repo repository) Exists(id string) (bool, error) {
	connection := repo.pool.Get()
	defer connection.Close()

	key := repo.key(id)
	ok, err := redis.Bool(connection.Do("EXISTS", key))
	if err != nil {
		return ok, fmt.Errorf("error checking if key %s exists: %v", key, err)
	}

	return ok, err
}

func (repo repository) Get(id string) ([]byte, error) {
	connection := repo.pool.Get()
	defer connection.Close()

	key := repo.key(id)
	response, err := redis.Bytes(connection.Do("GET", key))
	if err != nil {
		return []byte{}, err
	}

	return response, nil
}

func (repo repository) GetAll() ([][]byte, error) {
	connection := repo.pool.Get()
	defer connection.Close()

	products := [][]byte{}
	iter := 0
	pattern := repo.keyPrefix + "*"
	for {
		arr, err := redis.Values(connection.Do("SCAN", iter, "MATCH", pattern))
		if err != nil {
			return nil, fmt.Errorf("error retrieving '%s' keys", pattern)
		}

		iter, _ = redis.Int(arr[0], nil)
		keys, _ := redis.Strings(arr[1], nil)
		for _, key := range keys {
			id := repo.id(key)
			product, err := repo.Get(id)
			if err != nil {
				return nil, err
			}

			products = append(products, product)
		}

		if iter == 0 {
			break
		}
	}

	return products, nil
}

func (repo repository) key(id string) string {
	return fmt.Sprintf("%s:%s", repo.keyPrefix, id)
}

func (repo repository) id(key string) string {
	return key[len(repo.keyPrefix)+1 : len(key)]

}
