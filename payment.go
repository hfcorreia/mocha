package mocha

import (
	"fmt"
)

type paymentService struct {
	productService ProductService
	changeService  ChangeService
}

func NewPaymentService(productService ProductService, changeService ChangeService) PaymentService {
	return paymentService{productService, changeService}
}

func (service paymentService) Pay(product Product, amount Change) (Change, error) {
	if product.Price > float64(amount) {
		return 0, fmt.Errorf("insuficient funds to pay")
	}

	if product.Quantity == 0 {
		return 0, fmt.Errorf("insuficient products to return")
	}

	product.Quantity = product.Quantity - 1

	err := service.productService.Update(product)
	if err != nil {
		return 0, err
	}

	diff := Change(float64(amount) - product.Price)
	if diff == 0 {
		return 0, nil
	}

	err = service.changeService.Withdraw(diff)
	if err != nil {
		return 0, err
	}

	return diff, nil
}
