package mocha

import (
	"encoding/json"
	"fmt"
)

type changeService struct {
	repo Repository
}

func NewChangeService(repo Repository) ChangeService {
	return changeService{
		repo: repo,
	}
}

func (service changeService) SetInitialChange(amount float64) error {
	value, err := json.Marshal(Change(amount))
	if err != nil {
		return err
	}

	return service.repo.Set(changeID, value)
}

const changeID = "change"

func (service changeService) AddChange(change Change) error {
	exists, err := service.repo.Exists(changeID)
	if err != nil {
		return err
	}

	if exists {
		existingChange, err := service.Get()
		if err != nil {
			return err
		}

		change += existingChange
	}

	value, err := json.Marshal(change)
	if err != nil {
		return err
	}

	return service.repo.Set(changeID, value)
}

func (service changeService) Get() (Change, error) {
	stored, err := service.repo.Get(changeID)
	if err != nil {
		return 0, nil
	}

	var change Change
	err = json.Unmarshal(stored, &change)
	if err != nil {
		return 0, nil
	}

	return change, nil
}

func (service changeService) Withdraw(ammount Change) error {
	existingChange, err := service.Get()
	if err != nil {
		return err
	}

	change := existingChange - ammount
	if change < 0 {
		return fmt.Errorf("cannot withdraw %.2f from existing %.2f", ammount, existingChange)
	}

	value, err := json.Marshal(change)
	if err != nil {
		return err
	}

	return service.repo.Set(changeID, value)
}
