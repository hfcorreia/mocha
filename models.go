package mocha

import (
	"fmt"
)

type Product struct {
	Name     string
	Price    float64
	Quantity int
}

type Change float64

func NewChange(value float64) (Change, error) {
	validValues := []float64{.01, .02, .05, .10, .20, .50, 1, 2}

	for _, validVal := range validValues {
		if validVal == value {
			return Change(value), nil
		}

	}

	return 0, fmt.Errorf("%.2f is an invalid change type\n", value)
}
