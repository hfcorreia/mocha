# Mocha vending machine

### Dependencies

- Docker

## How to Run

Enter the docker:

```
$ docker run cmd
```

## Build the cmd

Inside the docker:

```
$ go build cmd/main.go
```

## Load Seed

Inside the docker:

```
$ ./main load
```

Seed file is at `cmd/app/seed`.

## Run Admin Mode

Inside the docker:

```
$ ./main admin
```

## Run Customer Mode

Inside the docker:

```
$ ./main customer
```