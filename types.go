package mocha

type Services struct {
	Products ProductService
	Change   ChangeService
	Payment  PaymentService
}
