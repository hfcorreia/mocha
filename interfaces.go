package mocha

type Repository interface {
	Set(id string, value []byte) error
	Exists(id string) (bool, error)
	Get(id string) ([]byte, error)
	GetAll() ([][]byte, error)
}

type ProductService interface {
	Create(name string, price float64, quantity int) error
	Update(product Product) error
	Get(name string) (Product, error)
	GetAll() ([]Product, error)
}

type ChangeService interface {
	SetInitialChange(amount float64) error
	AddChange(amount Change) error
	Withdraw(ammount Change) error
	Get() (Change, error)
}

type PaymentService interface {
	Pay(product Product, amount Change) (Change, error)
}
