package mocha

import (
	"encoding/json"
	"fmt"
)

type productService struct {
	repo Repository
}

func NewProductService(repo Repository) ProductService {
	return productService{
		repo: repo,
	}
}

func (service productService) Create(name string, price float64, quantity int) error {
	exists, err := service.repo.Exists(name)
	if err != nil {
		return err
	}

	if exists {
		return fmt.Errorf("product already exists for %s", name)
	}

	productToCreate := Product{name, price, quantity}
	value, err := json.Marshal(productToCreate)
	if err != nil {
		return err
	}

	return service.repo.Set(name, value)
}

func (service productService) Update(product Product) error {
	value, err := json.Marshal(product)
	if err != nil {
		return err
	}

	return service.repo.Set(product.Name, value)
}

func (service productService) Get(name string) (Product, error) {
	exists, err := service.repo.Exists(name)
	if err != nil {
		return Product{}, nil
	}

	if !exists {
		return Product{}, fmt.Errorf("couldn't find any product named %s", name)
	}

	value, err := service.repo.Get(name)
	if err != nil {
		return Product{}, err
	}

	var product Product
	err = json.Unmarshal(value, &product)
	if err != nil {
		return Product{}, err
	}

	return product, nil
}

func (service productService) GetAll() ([]Product, error) {
	values, err := service.repo.GetAll()
	if err != nil {
		return nil, err
	}

	var products []Product
	for _, value := range values {
		var product Product
		err = json.Unmarshal(value, &product)
		if err != nil {
			return nil, err
		}

		products = append(products, product)
	}

	return products, nil
}
